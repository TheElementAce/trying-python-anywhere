from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class UserProfileInfo(models.Model):

    user = models.OneToOneField(User)  # Adds on attributes to the User class (think of it as extending)

    # additional
    portfolio_site = models.URLField(blank=True)
    profile_pic = models.ImageField(upload_to='profile_pics', blank=True)  # profile_pics is in the media folder

    def __str__(self):
        return self.user.username